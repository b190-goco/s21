
//MUTATOR METHODS
let fruits = [], numbers = [1,2,3,4,6,7,5,8];

fruits.push("Orange");
let removedFruit = fruits.pop()
console.log(removedFruit);
//POP REMOVES LAST ELEMENT

fruits.unshift("Lime","Banana");
console.log("*UNSHIFT*");
console.log(fruits);

let fruitRemoved = fruits.shift();
console.log("*SHIFT*");
console.log(fruits);
//SHIFT REMOVED FIRST ELEMENT

fruits.splice(1,2,"Lime","Cherry");
console.log("*SPLICE*");
console.log(fruits);
//REPLACE ELEMENTS WITH (pos1,pos2,value1,value2)

fruits.sort();
console.log("*SORT*");
console.log(fruits);
//ARRANGES STRING ALPHABETICALLY 

numbers.sort();
console.log("*SORT(NUMBERS)*");
console.log(numbers);
//ARRANGES NUMBERS ASCENDING 

fruits.reverse();
console.log("*REVERSE*");
console.log(fruits);
//REVERSES ARRAY ORDER (NOT DESCENDING IF NOT SORTED)

//NON-MUTATOR METHODS
/*
	- functions do not modify original array
	- do not manipulate elements
	- return, combine, printing, etc.
*/
//indexOf()
let countries = ["RUS","CH","JPN","PH","USA","KOR","AUS","CAN","PH"]; 
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf(): "+firstIndex);
firstIndex = countries.indexOf("KAZ");
console.log("Result of indexOf(): "+firstIndex);
//RETURNS INDEX OF FIRST MATCH ELEMENT
/*
index is position of element minus 1
(ex. first element, PH, is FIRST but index is 0)
*/

let includesElement = countries.includes("USA");
console.log("Result of contains(): "+includesElement);
//DETERMINES IF INPUT IS FOUND IN ARRAY
//RETURNS TRUE AND FALSE
//***simpler version of indexOf() since it does not concern position

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf(): "+lastIndex);
lastIndex = countries.lastIndexOf("KAZ");
console.log("Result of lastIndexOf(): "+lastIndex);
//RETURNS INDEX (but started counting from last element);

let slicedArrayA = countries.slice(2);
console.log(countries);
console.log("Result of slice(): "+slicedArrayA);
//COPIES SLICED ARRAY INTO A NEW ONE
//CAN BE STORED IN A VARIABLE

let slicedArrayB = countries.slice(2,5);
console.log(countries);
console.log("Result of slice(): "+slicedArrayB);
//COPIED SPECIFIED (INDEX,POSITION) INTO NEW ARRAY

let slicedArrayC = countries.slice(-3);
console.log(countries);
console.log("Result of slice(): "+slicedArrayC);
// NEGATIVE INDEX COUNTS BACK FROM THE LAST ELEMENT
// STILL INCLUDED UNTIL END OF ARRAY (since no POS included)

let stringArray = countries.toString();
console.log(countries);
console.log("Result of toString(): " + stringArray);
//STORES ALL ELEMENTS INTO STRING (includes COMMA)

let tasksA = ["drink HTML", "eat Javscript"];
let tasksB = ["inhale CSS", "breath SASS"];
let tasksC = ["get GIT", "be node"];

let tasks = tasksA.concat(tasksB);
console.log("Result of tasksA.concat(tasksB)");
console.log(tasks);
// COMBINES ARRAYS

let allTasks = tasksA.concat(tasksB,tasksC);
console.log("Result of tasksA.concat(tasksB,tasksC)");
console.log(tasks);
// COMBINES ARRAY with CONCAT AND ALL ARRAY CONTAINED

let users = ["John","Jane","Joe","Jobert","Julius"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join('-'));
//JOINS ELEMENTS TOGETHER with INPUT in-between

allTasks.forEach(function(task){
	console.log(task);
})
//DOES SOMETHING TO EACH ELEMENT OF ARRAY

let filteredTasks = [];
allTasks.forEach(function(task){
	if (task.length > 10){
		filteredTasks.push(task);
	}
	//LIMITS THE PUSH COMMAND TO QUALIFIED ELEMENTS
})
console.log(allTasks);
console.log(filteredTasks);
//STORED PUSHED VALUES

numbers = [1,2,3,4,5];

let numbersMap = numbers.map(function(number){
	return Math.pow(number,2);
})
console.log(numbers);
console.log("Result of numbers.map(): ");
console.log(numbersMap);

let allValid = numbers.every(function(number){
	return (number<3);
})
console.log(numbers);
console.log("Result of numbers.every(): ");
console.log(allValid);
//must apply to ALL ELEMENTS

let someValid = numbers.some(function(number){
	return (number<2);
})
console.log(numbers);
console.log("Result of numbers.some(): ");
console.log(someValid);
//if there is 1, it is TRUE

let filterValid = numbers.filter(function(number){
	return (number<3);
})
console.log(numbers);
console.log("Result of numbers.filter(): ");
console.log(filterValid);
//returns ELEMENTS which are APPLIED
//can be stored to a variable

