console.log("Hello World");

/*MINIACTIVITY*/
/*
	array cities
		Tokyo
		Manila
		Seoul
		Jakarta
		Sim
	log
	send to chat
*/

let cities = 
[
"Tokyo",
"Manila",
"Seoul",
"Jakarta",
"Sim"
];
console.log(cities);
console.log(cities.length);
console.log(cities.length - 1);
console.log(cities[cities.length - 1]);

let blankArr = [];
console.log(blankArr);
console.log(blankArr.length);
console.log(blankArr[blankArr.length]);

console.log(cities);
cities.length--;
// chopped off last element (removed 1 element from end by reducing array size)
console.log(cities);
/*cities.length-=3;
// confirms decreasing array length removes elements
console.log(cities);
cities.length+=3;
// adds 3 slots but empty
console.log(cities);*/

let lakersLegends = ["Kobe","Shaq","Magic","Kareem","LeBron"];
console.log(lakersLegends);

/*let twodeearray = [[1,2,3],[2,3,1],[3,2,1]];
//basically making arrays and separating with ','
console.log(twodeearray);*/

/*MINIACTIVITY*/
/*
	replace last elemet with Pau Gasol
*/
lakersLegends.length++;
console.log(lakersLegends);
lakersLegends[lakersLegends.length-1]="Pau Gasol";
console.log(lakersLegends);
lakersLegends[lakersLegends.length]="Anthony Davis";
// can add elements to array without adding length
console.log(lakersLegends);

let numArray = [5,12,30,46,40];

for (let i = 0; i < numArray.length; i++){
	if (numArray[i]%5===0){
		console.log(numArray[i]+" is divisible by 5");
	}
	else {
		console.log(numArray[i]+" is NOT divisible by 5");
	}
}
for (let i = 0; i < numArray.length; i++){
	console.log(numArray[i]);
}
/*for (let i = 1; i <= numArray.length; i++){
	console.log(numArray[numArray.length-i]);
}
//my attempt*/
for (let i = numArray.length-1; i >= 0; i--){
	console.log(numArray[i]);
}
//encodes from last element

let chessBoard = [
/*[[[1,2],2,3,4],"b1","c1","d1","e1"],*/
["a1","b1","c1","d1","e1"],
["a2","b2","c2","d2","e2"],
["a3","b3","c3","d3","e3"],
["a4","b4","c4","d4","e4"],
["a5","b5","c5","d5","e5"],
]

console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log("PAWN moves to "+chessBoard[2][3])